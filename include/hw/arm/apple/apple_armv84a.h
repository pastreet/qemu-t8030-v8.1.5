#pragma once

#include "qemu/osdep.h"
#include "qemu/typedefs.h"

#include "hw/boards.h"
#include "hw/sysbus.h"

#include "hw/arm/apple/t8030_cpu.h"
#include "hw/arm/apple/xnu.h"

typedef struct {
    /*< private >*/
    MachineClass parent_class;
    /*< public >*/
} ARMV84AMachineClass;

typedef enum BootMode {
    kBootModeAuto = 0,
    kBootModeManual,
    kBootModeEnterRecovery,
    kBootModeExitRecovery,
} BootMode;

typedef struct {
    MachineState parent;
    hwaddr soc_base_pa;
    hwaddr soc_size;

    unsigned long dram_size;
    T8030CPUState *cpus[T8030_MAX_CPU];
    T8030CPUCluster clusters[T8030_MAX_CLUSTER];
    SysBusDevice *aic;
    MemoryRegion *sysmem;
    struct mach_header_64 *kernel;
    DTBNode *device_tree;
    struct macho_boot_info bootinfo;
    video_boot_args video;
    char *trustcache_filename;
    char *ticket_filename;
    // char *usbfuzz_filename;
    // bool usbfuzz;
    BootMode boot_mode;
    uint32_t build_version;
    Notifier init_done_notifier;
    hwaddr panic_base;
    hwaddr panic_size;
} ARMV84AMachineState;

#define TYPE_ARM84A_APPLE "armv8.4a"
#define TYPE_ARM84A_MACHINE MACHINE_TYPE_NAME(TYPE_ARM84A_APPLE)
DECLARE_OBJ_CHECKERS(ARMV84AMachineState, ARMV84AMachineClass, ARM84A_MACHINE,
                     TYPE_ARM84A_MACHINE)
