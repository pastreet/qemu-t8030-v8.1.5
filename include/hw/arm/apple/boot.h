#pragma once

#include "qemu/osdep.h"

#include "hw/arm/apple/apple_armv84a.h"
#include "hw/arm/apple/xnu.h"

bool xnu_machine_load_kernel(struct mach_header_64 **pkernel,
                             uint32_t *pbuild_version, MachineState *machine,
                             uint64_t *pkernel_low, uint64_t *pkernel_high);
