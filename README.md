# Introduction

This repository is a fork of qemu at the `v8.1.5` tag. It incorporates knowledge from multiple sources to create a
`v8.1.5` based iPhone fuzzer. The changes incorporated here-in are meant to be run in concern with its sister 
repository. See https://gitlab.com/pastreet/libafl-t8030-ios14/-/tree/t8030-ios14-fuzzer/fuzzers/qemu_t8030_ios14?ref_type=heads

# Native Command

The following command should demonstrate that we have a functioning emulated kernel in QEMU. This will be the same
state of affairs that you could observe if you were to directly clone TrungNguyen1909's `qemu-t8030`[2].

```
./qemu/build/qemu-system-aarch64 -icount shift=auto,align=off,sleep=off \
     -machine iphone11-t8030,trustcache-filename=Firmware/038-44135-124.dmg.trustcache,ticket-filename=rootticket.der,boot-mode=enter_recovery \
		 -cpu max -smp 1 -d unimp,guest_errors \
     -monitor null -serial stdio \
		 -monitor telnet:127.0.0.1:12345,server,nowait \
     -kernel kernelcache.research.iphone12b \
     -initrd ios_scfuzzer.dmg \
     -dtb Firmware/all_flash/DeviceTree.n104ap.im4p \
     -nographic -snapshot -S \
     -append "debug=0x14e kextlog=0xffff serial=3 -v launchd_unsecure_cache=1 tlto_us=300000 wdt=-1 iomfb_system_type=2 iomfb_disable_rt_bw=1" \
     -drive if=nvme-ns,index=0,id=drive.1,format=qcow2,file=ramdisk/nvme.1.qcow2 \
     -drive if=nvme-ns,index=1,id=drive.2,format=qcow2,file=ramdisk/nvme.2.qcow2 \
     -drive if=nvme-ns,index=2,id=drive.3,format=qcow2,file=ramdisk/nvme.3.qcow2 \
     -drive if=nvme-ns,index=3,id=drive.4,format=qcow2,file=ramdisk/nvme.4.qcow2 \
     -drive if=nvme-ns,index=4,id=drive.5,format=qcow2,file=ramdisk/nvram.qcow2 \
     -drive if=nvme-ns,index=5,id=drive.6,format=qcow2,file=ramdisk/nvme.6.qcow2 \
     -drive if=nvme-ns,index=6,id=drive.7,format=qcow2,file=ramdisk/nvme.7.qcow2
```

See: https://github.com/PacktPublishing/Fuzzing-Against-the-Machine/tree/main/Chapter_10/setup-ios and its book for more details.

# Prior Work

All credit goes to previous giants. Thanks so much for publishing your work so that others (me) could benefit! :)

1. Fuzzing Against the Machine; Nappa, Antonio & Blazquez, Eduardo : motivating resource
2. https://github.com/TrungNguyen1909/qemu-t8030  : now archived, implements all/most of what we seek to implement but forked off of another point
3. https://github.com/flogosec/qemu-avr32/tree/avr32 : project implementing a new architecture on QEMU on a more recent forking point
4. https://fgoehler.com/blog/adding-a-new-architecture-to-qemu-01/ : Göhler's blog guiding the early state of the AVR32 project
5. https://airbus-seclab.github.io/qemu_blog/ : earlier foundation work published by Airbus Security Lab; authored by Stéphane Duverger
