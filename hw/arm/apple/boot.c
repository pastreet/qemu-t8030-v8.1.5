#include "hw/arm/apple/boot.h"

#include <stdio.h>

#include "exec/cpu-defs.h"

bool xnu_machine_load_kernel(struct mach_header_64 **pkernel,
                             uint32_t *pbuild_version, MachineState *machine,
                             uint64_t *pkernel_low, uint64_t *pkernel_high) {
    if (pkernel == NULL || pbuild_version == NULL ||
        machine == NULL || pkernel_low == NULL || pkernel_high == NULL) {
        return false;
    }

    fprintf(stderr, "Loading %s...\n", machine->kernel_filename);
    struct mach_header_64 *hdr = macho_load_file(machine->kernel_filename);
    if (hdr == NULL) {
        return false;
    }

    fprintf(stderr, "kernel loaded!\n");
    *pkernel = hdr;
    *pbuild_version = macho_build_version(hdr);
    fprintf(stderr, "Loading %s %u.%u...\n", macho_platform_string(hdr),
            BUILD_VERSION_MAJOR(*pbuild_version),
            BUILD_VERSION_MINOR(*pbuild_version));
    macho_highest_lowest(hdr, pkernel_low, pkernel_high);

    fprintf(stderr,
            "kernel_low: 0x" TARGET_FMT_lx "\nkernel_high: 0x" TARGET_FMT_lx
            "\n",
            *pkernel_low, *pkernel_high);
    return true;
}
